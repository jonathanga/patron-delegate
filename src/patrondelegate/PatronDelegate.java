/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patrondelegate;

/**
 *
 * @author ADMIN
 */
public class PatronDelegate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado objEmpleado = new Empleado(new ClaseCodificadora(),new ClaseDisenadora());
        objEmpleado.codificar();
        objEmpleado.disenar();
    }
    
}
