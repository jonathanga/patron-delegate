/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patrondelegate;

/**
 *
 * @author ADMIN
 */
public class Empleado implements ICodificadora,IDisenadora{
    
    ICodificadora codificador;
    IDisenadora disenador;

    public Empleado(ICodificadora codificador, IDisenadora disenador) {
        this.codificador = codificador;
        this.disenador = disenador;
    }

    @Override
    public void codificar() {
        codificador.codificar();
    }

    @Override
    public void disenar() {
        disenador.disenar();
    }
    
}
